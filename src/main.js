import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import './assets/main.css';
import './assets/iconfont/iconfont.css';
import './assets/iconfont/iconfont.js';

const app = createApp(App);
app.use(router).use(store).use(ElementPlus);
app.mount('#app');
