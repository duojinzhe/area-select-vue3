import { createStore } from 'vuex';

export default createStore({
  state: { IsMenu: false },
  mutations: {
    setIsMenu(state, val) {
      state.IsMenu = val;
    },
  },
  actions: {},
  modules: {},
});
